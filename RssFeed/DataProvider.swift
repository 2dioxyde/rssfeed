//
// Created by Arseny on 8/26/20.
// Copyright (c) 2020 arseny. All rights reserved.
//

import Foundation

class DataProvider {

    private let sourcesListKey = "Sources"
    private let sourceKey = "savedSource"
    private let readKey = "readPost"
    private let defaultSources = ["https://www.banki.ru/xml/news.rss", "https://www.finam.ru/net/analysis/conews/rsspoint"]
    private var completion: (([Post], String, [Int]) -> ())?
    private let defaults = UserDefaults.standard

    private func getSource() -> String {
        defaults.string(forKey: sourceKey) ?? defaultSources[0]
    }
}

extension DataProvider: FeedDataProvider {

    func getFeed(_ completion: @escaping ([Post], String, [Int]) -> ()) {
        self.completion = completion
        let source = getSource()
        Parser().parse(source) { posts in
            let ids = self.getReadPostsIds(posts)
            completion(posts, source, ids)
        }
    }

    func markAsRead(_ post: Post) {
        var read = (defaults.array(forKey: readKey) ?? []) as [String]
        if !read.contains(post.title) {
            read.append(post.title)
            defaults.set(read, forKey: readKey)
        }
    }

    func getReadPostsIds(_ posts: [Post]) -> [Int] {
        let read = (defaults.array(forKey: readKey) ?? []) as [String]
        var ids: [Int] = []
        for (index, post) in posts.enumerated() {
            if read.contains(post.title) {
                ids.append(index)
            }
        }
        return ids
    }
}

extension DataProvider: SourceDataProvider {

    func getSources() -> [String] {
        defaults.array(forKey: sourcesListKey) as? [String] ?? defaultSources
    }

    func addSource(_ source: String) {
        var sources = getSources()
        sources.append(source)
        defaults.set(sources, forKey: sourcesListKey)
    }

    func removeSource(at index: Int) {
        var sources = getSources()
        sources.remove(at: index)
        defaults.set(sources, forKey: sourcesListKey)
    }

    func selectSource(_ source: String) {
        defaults.set(source, forKey: sourceKey)

        if let completion = completion {
            getFeed(completion)
        }
    }
}

protocol FeedDataProvider {
    func getFeed(_ completion: @escaping ([Post], String, [Int]) -> Void)
    func markAsRead(_ post: Post)
}

protocol SourceDataProvider {

    func getSources() -> [String]
    func addSource(_ source: String)
    func removeSource(at index: Int)
    func selectSource(_ source: String)
}