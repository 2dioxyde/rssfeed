//
// Created by Arseny on 8/26/20.
// Copyright (c) 2020 arseny. All rights reserved.
//

import Foundation

class Parser: NSObject, XMLParserDelegate {

    var posts: [Post] = []
    var parser = XMLParser()
    var tempPost: Post? = nil
    var tempElement: String?
    var completion: (([Post]) -> Void)?

    func parse(_ source: String, completion: @escaping ([Post]) -> Void) {
        self.completion = completion
        parser = XMLParser(contentsOf: (NSURL(string: source))! as URL)!
        parser.delegate = self
        parser.parse()
    }

    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("parse error: \(parseError)")
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String: String]) {
        tempElement = elementName
        if elementName == "item" {
            tempPost = Post(title: "", description: "", date: "")
        }
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            if let post = tempPost {
                posts.append(post)
            }
            tempPost = nil
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if let post = tempPost {
            if tempElement == "title" {
                tempPost?.title = post.title + string
            } else if tempElement == "description" {
                tempPost?.description = post.description + string
            } else if tempElement == "pubDate" {
                tempPost?.date = post.date + string
            }
        }
    }

    func parserDidEndDocument(_ parser: XMLParser) {
        completion?(posts.sorted {
            $0.date > $1.date
        })
    }
}