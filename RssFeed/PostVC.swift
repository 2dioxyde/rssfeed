//
// Created by Arseny on 8/22/20.
// Copyright (c) 2020 arseny. All rights reserved.
//

import Foundation
import UIKit

class PostVC: UIViewController {

    var post: Post?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        view.backgroundColor = .gray

        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false

        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false

        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.numberOfLines = 0
        title.font = .preferredFont(forTextStyle: .title2)

        let date = UILabel()
        date.translatesAutoresizingMaskIntoConstraints = false
        date.font = .preferredFont(forTextStyle: .subheadline)

        let description = UITextView()
        description.translatesAutoresizingMaskIntoConstraints = false
        description.isScrollEnabled = false
        description.isEditable = false
        description.isUserInteractionEnabled = true
        description.isSelectable = true
        description.dataDetectorTypes = [UIDataDetectorTypes.link]
        description.backgroundColor = .clear

        container.addSubview(title)
        container.addSubview(date)
        container.addSubview(description)
        scrollView.addSubview(container)
        view.addSubview(scrollView)

        NSLayoutConstraint.activate([

            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            container.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            container.topAnchor.constraint(equalTo: scrollView.topAnchor),
            container.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            container.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            container.widthAnchor.constraint(equalTo: view.widthAnchor),

            title.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 8),
            title.topAnchor.constraint(equalTo: container.topAnchor, constant: 8),
            title.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -8),

            date.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 8),
            date.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 8),
            date.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -8),

            description.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 8),
            description.topAnchor.constraint(equalTo: date.bottomAnchor, constant: 8),
            description.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -8),
            description.bottomAnchor.constraint(equalTo: container.bottomAnchor)
        ])

        if let post = post {
            title.text = post.title
            date.text = post.date

            do {
                let text = try NSMutableAttributedString(
                        data: post.description.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                        options: [.documentType: NSAttributedString.DocumentType.html],
                        documentAttributes: nil)

                description.attributedText = text
                description.font = UIFont.preferredFont(forTextStyle: .body)
            } catch {
                print("")
            }
        }
    }
}