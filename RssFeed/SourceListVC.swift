//
// Created by Arseny on 8/24/20.
// Copyright (c) 2020 arseny. All rights reserved.
//

import Foundation
import UIKit

class SourceListVC: UITableViewController {

    var sourceProvider: SourceDataProvider!
    var sources: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        sources = sourceProvider.getSources()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
    }

    @objc func addTapped() {
        let alert = UIAlertController(title: "Add source", message: nil, preferredStyle: .alert)

        alert.addTextField { field in
            field.placeholder = "https://.."
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] _ in
            if let alert = alert,
               let textField = alert.textFields?[0],
               let text = textField.text,
               !text.isEmpty {
                self.sources.append(text)
                self.sourceProvider.addSource(text)
                self.tableView.reloadData()
            }
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))

        present(alert, animated: true)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sources.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = sources[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            sources.remove(at: indexPath.row)
            sourceProvider.removeSource(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sourceProvider.selectSource(sources[indexPath.row])
        navigationController?.popViewController(animated: true)
    }
}