//
// Created by Arseny on 8/22/20.
// Copyright (c) 2020 arseny. All rights reserved.
//

import Foundation

struct Post {
    var title: String!
    var description: String!
    var date: String!
}