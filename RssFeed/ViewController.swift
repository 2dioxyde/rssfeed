//
//  ViewController.swift
//  RssFeed
//
//  Created by Arseny on 8/22/20.
//  Copyright © 2020 arseny. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    var posts: [Post] = []
    var dataProvider: FeedDataProvider!
    var read: [Int] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        dataProvider = DataProvider()
        dataProvider.getFeed() { posts, source, ids in
            self.posts = posts
            self.read = ids
            self.navigationItem.title = source
            self.refresh()
        }

        tableView.backgroundColor = .gray

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Source", style: .plain, target: self, action: #selector(addTapped))

        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
    }

    @objc func addTapped() {
        let vc = SourceListVC()
        vc.sourceProvider = (dataProvider as! SourceDataProvider)
        navigationController?.pushViewController(vc, animated: false)
    }

    @objc func refresh() {
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        posts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.textLabel?.text = posts[indexPath.row].title
        cell.textLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = posts[indexPath.row].date
        cell.backgroundColor = .clear

        if read.contains(indexPath.row) {
            cell.backgroundColor = .lightGray
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row]
        let vc = PostVC()
        vc.post = post
        navigationController?.pushViewController(vc, animated: false)
        dataProvider.markAsRead(post)
        read.append(indexPath.row)
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}

